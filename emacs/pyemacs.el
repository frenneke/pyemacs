
;;;;;;;;;;;;;;;;
; Call Python
;;;;;;;;;;;;;;;;

;(setq python_exec "C:/Users/RennekeF/AppData/Local/Programs/Python/Python38-32/python.exe")
(setq python_exec "python3")
;(setq main_path "C:/Users/RennekeF/Documents/Projects/2020/Development/FTL Jira/pyemacs/main.py")
(setq main_path "/Users/florianrenneke/Documents/Programmierung/pyemacs/main.py")

(message "[INFO] Python Interface loading ...")

(defun eval-string (string)
"Evaluate a string as elisp code"
(interactive)
 (eval (car (read-from-string (format "(progn %s)" string)))))

(defun exec-python(file)
"Execute a python file and give back the response"
(interactive)
  (message "[INFO] Execute %s" (format "%s %s" python_exec file))
(shell-command-to-string (format "%s %s" python_exec file))
)

(defun eval-python (file)
"Evaluate the stdout response of a python file"
  (interactive)
  (message "[INFO] Eval file: %s" file)
 (eval-string (exec-python file))
)


(eval-python (format "\"%s\"" main_path))

(provide 'pyemacs)
