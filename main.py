import pyemacs as em
import os
import json
import re


store = em.store
ROOT = os.path.abspath(os.path.dirname(__file__))



def toolbar_reload():
    """
    Reload the python code
    """
    em.emacs_message("[INFO] Reloading emacs.py")
    em.emacs_reload()
    
def pyCallback(src, vars):
    print(f"HELLO ORG: {src}")
    
def emacsCallback_toogleSpeedbar():
    em.emacs_toggleSpeedbar()
    
    
    
def pyToggleItem(name, history):
    m = re.findall(r"/([^/]*)", history)
    x = store['speedbar']
    for key in m:
        if x!=store['speedbar'] and 'data' in x:
            x = x['data']
        x = x[key]
    
    if isinstance(x, str):
        em.emacs_insert(f"[[{x}]]\n")
        return
    
    x['folded'] = x['folded']==False
    
    emacs_draw_speedbar()

def emacs_item_clicked(title, text, level):
    em.emacs_callPython(pyToggleItem, name=title, history=text)
    em.emacs_callPython(emacs_draw_speedbar)
        
        

def recursive_speedbar(s, item, history="", level=1):
    
    for key, value in item.items():
        if not isinstance(value, str):
            # needs toplevel
            s.insert_tag(key, history+"/"+key, emacs_item_clicked, level=level, type="statictag", icon="nil")
            if not value['folded']:
                recursive_speedbar(s, value['data'], history+"/"+key, level+1)
        else:
            # is item
            s.insert_tag(key, history+"/"+key, emacs_item_clicked, level=level, type="statictag", icon="nil")
            
def emacs_draw_speedbar():

    with em.SpeedbarUpdate() as s:
        s.clear()
        s.insert("AVENUE Speedbar\n================\n")
        recursive_speedbar(s, store['speedbar'])

    
def emacs_speedbar(buffer, delfunc="delfunc"):
    emacs_draw_speedbar()






def main():
    em.emacs_require("sr-speedbar")
    em.emacs_require("active-buffer")
    em.emacs_loadTheme(os.path.join(ROOT, "emacs"), "zenburn")
        
    em.emacs_addPath("/Library/TeX/texbin")
    
    em.emacs_setTitle("FTL 2.0")
    
    # Register all functions
    em.pyFunctionToLisp(emacs_draw_speedbar)
    
    em.emacs_addToolbarItem("right-arrow", toolbar_reload, "Reload Python")
    
    em.emacs_addToolbarItem("left-arrow",
                            emacsCallback_toogleSpeedbar,
                            "Toggle Speedbar")
    
    em.emacs_setFrameWidth(140)
    
    em.emacs_addBabelExecute("ifxsda", pyCallback)
    em.emacs_setupBabelLanguages("shell", "python")
    em.emacs_noConfirmationForLang("ifxsda", "elisp", "shell", "python")

    em.emacs_disableSplashScreen()
    #em.emacs_disable_save_confirmation()
    
    #em.emacs_removeMenu("tools")
    #em.emacs_removeMenu("options")
    #em.emacs_removeMenu("buffer")

    em.emacs_openSpeedbar()
    em.enableLineWrapping()
    em.enableHighlightingMatchingParenthesis()
    
    
    em.emacs_openNewBuffer("funnyName")
    em.emacs_insert("* Hello\nThis is a test\n")
    em.emacs_setProperty("DATE", "05.05.2010")
    em.emacs_setProperty("DATE1", "05.05.2010")
    em.emacs_setProperty("DATE2", "05.05.2010")
    em.emacs_deletePropertyGlobally("DATE1")
    em.emacs_addSpeedbarDisplay("avenue", emacs_speedbar)
    
    print("""
    (defun org-global-props (&optional property buffer)
    "Get the plists of global org properties of current buffer."
    (unless property (setq property "PROPERTY"))
    (with-current-buffer (or buffer (current-buffer))
      (org-element-map (org-element-parse-buffer) 'keyword
      (lambda (el) (when (string-match property
      (org-element-property :key el)) el)))))
      
    (mapcar (lambda (prop)
      (list (org-element-property :key prop)
        (org-element-property :value prop)))
    (org-global-props "\\(AUTHOR\\|TITLE\\|XYZ\\)"))
    """)
    
if __name__ == "__main__":
    em.pyEmacsMain(globals())
