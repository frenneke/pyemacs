"""
Author: Florian Renneke
Date: 3.09.2020
"""

import os
import sys
import argparse
import json
import inspect
import re


def emacs_message(msg):
    print(f"(message \"{msg}\")")

def pyFunctionToLisp(emacs_function):
    argv = inspect.getfullargspec(emacs_function).args
    argv_str = " ".join(argv)
    print(f"(defun {emacs_function.__name__} ({argv_str})")
    doc = emacs_function.__doc__
    if(doc):
        print(f"\"{doc.strip()}\"")
    else:
        print("\"No description\"")
    print("(interactive)")
    argv_dict = {}
    for a in argv:
        argv_dict = {**argv_dict, a.replace("-", "__"): a}
    emacs_function(**argv_dict)
    print(")")

def emacs_latexExportAndOpenPDF():
    emacs_message("[INFO] Create PDF and open")
    print("(org-open-file (org-latex-export-to-pdf))\n")

def emacs_addToolbarItem(icon, emacs_callback, help):
    pyFunctionToLisp(emacs_callback)
    print(f"(tool-bar-add-item \"{icon}\" '{emacs_callback.__name__} '{emacs_callback.__name__} :help \"{help}\")")


def emacs_reload():
    print("(eval-python (format \"\\\"%s\\\"\" main_path))")


def emacs_noConfirmationForLang(*lang):
    text = " ".join([f"\"{x}\"" for x in lang])
    print(f"""
    (defun my-org-confirm-babel-evaluate (lang body)
      (not (member lang '({text}))))
    (setq org-confirm-babel-evaluate 'my-org-confirm-babel-evaluate)
    """)
    
def emacs_setupBabelLanguages(*lang):

    text2 = " ".join([f"( {x} . t )" for x in lang])
    
    print(f"""
    (org-babel-do-load-languages 'org-babel-load-languages '(
      {text2}
    ))
    """)


def emacs_callPython(pyCallback, **args):
    """
    Call a python function from emacs
    args will reference variables which must be existing on emacs side
    
    python main.py --function test_fun arg='content of arg'
    """
    path = os.path.abspath(pyCallback.__globals__['__file__'])
    path = path.replace("\\", "/") # For windows
    arg = ""
    arg2 = ""
    for k, v in args.items():
        arg += f"{k}='%s' "
        arg2 += f"{v} "
        
    print(f"""
    (setq cmd (format "\\"{path}\\" --function {pyCallback.__name__} {arg}" {arg2}) )
    (eval-string (exec-python cmd))
    """)

def emacs_addBabelExecute(name, pyCallback):
    """
    Add a custom babel section:
    #+BEGIN_SRC <name> :var ... :results raw
    #+END_SRC
    
    The following function will be called (arguments must match!):
    def callback(src, vars):
        print("Result")
        
    src: temp file path to the content of the src block
    vars: variables
        
    """
    path = os.path.abspath(pyCallback.__globals__['__file__'])
    path = path.replace("\\", "/") # For windows
    
    print(f"""
    (require 'ob)
    (defun org-babel-execute:{name} (body params)
     (let* ((full-body (org-babel-expand-body:generic body params))
           (src-file (org-babel-temp-file "{name}-" ".tmp"))
           (vars (org-babel--get-vars params))
     )
     (with-temp-file  src-file (insert full-body))
     (setq cmd (format "\\"{path}\\" --function {pyCallback.__name__} vars='%s' src='%s'" vars src-file) )
     (org-babel-read (exec-python cmd))
    )
    )
    ; Setting default variables so that it gets evaluated as org code
    (setq org-babel-default-header-args:{name}
    '((:results . "raw replace") (:exports . "both") (:cache . "no")))
    """)
    
def emacs_addPath(path):
    path = path.replace("\\", "\\\\") # For windows
    print(f"""(setenv "PATH" (concat "{path}" ":" (getenv "PATH")))""")



def emacs_loadTheme(dir, themename):
    dir = dir.replace("\\", "\\\\") # For windows
    print(f"""
    (add-to-list 'custom-theme-load-path "{dir}")
    (load-theme '{themename} t)
    """)

def emacs_require(package):
    print(f"(require '{package})")

def enableLineWrapping():
    print("(global-visual-line-mode) (setq visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow))")

def enableHighlightingMatchingParenthesis():
    print("(show-paren-mode 1)")

def emacs_setTitle(title):
    print(f"(setq frame-title-format \"{title}\")")

def emacs_setFrameWidth(width):
    print(f"(set-frame-width    (selected-frame) {width})")

def emacs_removeMenu(key):
    print(f"(define-key global-map [menu-bar {key}] nil)")

def emacs_disableSplashScreen():
    print("(setq inhibit-startup-screen t)")

def emacs_disable_save_confirmation():
    print("(defun save-some-buffers (&optional arg pred) nil)")


def emacs_toggleSpeedbar():
    print("(sr-speedbar-toggle)")
    
def emacs_openSpeedbar():
    print("(sr-speedbar-open)")
    
def emacs_closeSpeedbar():
    print("(sr-speedbar-close)")
    
    
def emacs_insert(txt, plain=False):
    if not plain:
        txt = f"\"{txt}\""
    print(f""" 
    (with-current-buffer (pyemacs-get-active-buffer)
       (insert {txt})
    )
    """)



class SpeedbarUpdate:
    def __init__(self):
        pass
    def __enter__(self):
        print("(speedbar-with-writable (save-excursion ")
        return self
    def clear(self):
        print("(erase-buffer)")
    def insert(self, text):
        print(f"""
        (insert "{text}")
        """)
        
    def insert_tag(self, label, userdata, emacs_callback, level=1, type="statictag", icon="nil"):
        """
        Insert a row to the speedbar.
        This function has to be called inside of the speedbar redraw callback
        """
        pyFunctionToLisp(emacs_callback)
        print(f"""
        (speedbar-make-tag-line '{type}
                      {icon} nil
                      nil
                      "{label}"
                '{emacs_callback.__name__}
                "{userdata}"
                      'speedbar-tag-face
                {level})
        
        """)
        
    def change_button_char(self, char):
        print(f"(speedbar-change-expand-button-char {char})")
        
        
    def __exit__(self, type, value, tb):
        print("))")
    
    
    
def emacs_openNewBuffer(name="untitled"):
    print(f"""
    (switch-to-buffer (generate-new-buffer "{name}"))
    (org-mode)
    (put 'buffer-offer-save 'permanent-local t)
    (setq buffer-offer-save t)
    """)


def emacs_addSpeedbarDisplay(name, emacs_callback):

    print(f"""
    ;
    ; Create Speedbar display {name} - start
    ;
    """)
    
    
    pyFunctionToLisp(emacs_callback)

    print(f"""    
    (speedbar-change-initial-expansion-list "{name}")
            
    (defun {name}-variables ()
      "Install speedbar variables."
        (setq {name}-speedbar-key-map (speedbar-make-specialized-keymap))
     )

    (if (featurep 'speedbar)
        (progn
          ({name}-variables)
        )
      (add-hook 'speedbar-load-hook '{name}-varibles)
    )

    (speedbar-add-expansion-list '("{name}" 
                 nil
                 {name}-speedbar-key-map
                 {emacs_callback.__name__}
                 ))

    ;
    ; Create Speedbar display {name} - end
    ;
    """)





#######################
# Store for variables
#######################
global store, store_path
store_path = "~/.pymacs_store.json"
if os.getenv("PYEMACS_STORE"):
    store_path = os.getenv("PYEMACS_STORE")
    
def save():
    global store, store_path
    with open(store_path, "w") as f:
        f.write(json.dumps(store, indent=4))
def restore():
    global store, store_path
    if not os.path.isfile(store_path):
        save()
    else:
        with open(store_path, "r") as f:
            store=json.load(f)

restore() # Restore variables








def emacs_deletePropertyGlobally(key):
    print(f"""
    (org-delete-property-globally "{key}")
    """)


def emacs_deleteProperty(key):
    print(f"""
    (org-delete-property "{key}")
    """)


def emacs_setProperty(key, value):
    print(f"""
    (org-set-property "{key}" "{value}")
    """)


def emacs_registerNewProp(key):
    printf(f"""
    """)

def emacs_getProperty(key):
    print(f"""
    (org-element-property :value (car (org-global-props {key})))
    """)


def pyEmacsMain(globals):
    
    parser = argparse.ArgumentParser(description='Emacs Python Interface')
    parser.add_argument('--function', default="", help="If given, call this function")
    parser.add_argument('args', nargs=argparse.REMAINDER)
    
    args = parser.parse_args()

    if args.function == "":
        if "main" not in globals:
            raise Exception(f"Function main() does not exist")
        
        globals['main']()
    else:
        # Call specific function
        if args.function not in globals:
            raise Exception(f"Function {args.function}() does not exist")
        
        args_list = vars(args)['args']
        arg_string = ""
        for a in args_list:
            m = re.findall(r"\s*([^,= ]*)\s*=\s*([^,]*)", a)
            if m:
                for f in m:
                    arg_string += f"{f[0]}=\"{f[1]}\","
        if arg_string:
            arg_string = arg_string[:-1]
            
        eval(f"globals['{args.function}']({arg_string})")
        
    save() # Save variables
