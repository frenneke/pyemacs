
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Getting the main buffer (not starting with *)
(defvar sb-imenu-active-buffer nil)

(defun pyemacs-get-active-buffer ()
  "Get the active buffer."
  (setq sb-imenu-active-buffer nil)
  (condition-case nil
      (with-selected-frame (dframe-select-attached-frame (speedbar-current-frame))
        (sb-imenu-get-interesting-buffer))
    (error nil))
  (unless sb-imenu-active-buffer
    (sb-imenu-get-interesting-buffer))
)
    
(defun sb-imenu-get-interesting-buffer ()
  "Get an interesting buffer."
  (catch 'done
    (dolist (buffer (buffer-list))
      (unless (string-match "^[ *]" (buffer-name buffer))
        (setq sb-imenu-active-buffer buffer)
        (throw 'done buffer)))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'active-buffer)
